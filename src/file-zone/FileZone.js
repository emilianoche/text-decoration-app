import React, { Component } from 'react';
import './FileZone.css';

class FileZone extends Component {
  
    render() {
        return (
            <div id="file-zone">
                <div id="file" onDoubleClick={e=>this.props.selectWorld(e)}>
                    {this.props.text}
                </div>
            </div>
        );
    }
}

export default FileZone;
