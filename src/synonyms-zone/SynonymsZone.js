import React, { Component } from 'react';

class ControlPanel extends Component {
    render() {
        return (
            <div>
                <h1>
                Synonyms
                </h1>
                <div>
                  <ul>
                    {this.props.synonyms.data && this.props.synonyms.data.map(value=>{
                      return <li>{value.word}<button onClick={()=>this.props.replaceWord(value.word)}>change</button></li>
                    })}
                  </ul>
                  <button type="button" onClick={()=>this.props.getSynonym()}>Get Synonym</button>
                </div>
            </div>
        );
    }
}

export default ControlPanel;
