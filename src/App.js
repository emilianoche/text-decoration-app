import React, {Component} from 'react';
import './App.css';
import ControlPanel from "./control-panel/ControlPanel";
import FileZone from "./file-zone/FileZone";
import SynonymsZone from "./synonyms-zone/SynonymsZone";
import getMockText from './text.service';
import axios from 'axios'

class App extends Component {
  state = {word:'', text: '', synonyms:[]}
    componentDidMount() {
        getMockText().then((result) =>
            this.setState({text:result})
        );
    }
    
  selectWorld = (e) =>{
    e.preventDefault();
    this.setState({word:window.getSelection().toString()})
  }

  getSynonym = () => {
    axios.get(`https://api.datamuse.com/words?rel_syn=${this.state.word}&max=4`).then(value=>this.setState({synonyms:value}))
  }

  replaceSelectedText =(replacementText) => {
    var sel, range;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            range.insertNode(document.createTextNode(replacementText));
        }
    } else if (document.selection && document.selection.createRange) {
        range = document.selection.createRange();
        range.text = replacementText;
    }
}
  changeToBold = () => {
    let sel;
    let range;
    sel = window.getSelection();
  if (sel.rangeCount && sel.getRangeAt) {
    range = sel.getRangeAt(0);
  }
  // Set design mode to on
  document.designMode = "on";
  if (range) {
    sel.removeAllRanges();
    sel.addRange(range);
  }
  // Colorize text
  document.execCommand("bold");
  // Set design mode to off
  document.designMode = "off";
  }

  changeToItalic = () => {
    let sel;
    let range;
    sel = window.getSelection();
  if (sel.rangeCount && sel.getRangeAt) {
    range = sel.getRangeAt(0);
  }
  // Set design mode to on
  document.designMode = "on";
  if (range) {
    sel.removeAllRanges();
    sel.addRange(range);
  }
  // Colorize text
  document.execCommand("italic");
  // Set design mode to off
  document.designMode = "off";
  }

  changeToUnderline = () => {
    let sel;
    let range;
    sel = window.getSelection();
  if (sel.rangeCount && sel.getRangeAt) {
    range = sel.getRangeAt(0);
  }
  // Set design mode to on
  document.designMode = "on";
  if (range) {
    sel.removeAllRanges();
    sel.addRange(range);
  }
  // Colorize text
  document.execCommand("underline");
  // Set design mode to off
  document.designMode = "off";
  }

    render() {
        return (
            <div className="App">
                <header>
                    <span>Simple Text Editor</span>
                </header>
                <main>
                    <ControlPanel changeToBold={this.changeToBold} changeToItalic={this.changeToItalic} changeToUnderline={this.changeToUnderline}/>
                    <SynonymsZone getSynonym={this.getSynonym} replaceWord={this.replaceSelectedText} synonyms={this.state.synonyms}/>
                    <FileZone text={this.state.text} selectWorld={this.selectWorld}/>
                </main>
            </div>
        );
    }
}

export default App;
